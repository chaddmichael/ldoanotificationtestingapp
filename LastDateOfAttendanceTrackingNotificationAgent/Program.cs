﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LastDateOfAttendanceTrackingNotificationAgent.Data;
using LastDateOfAttendanceTrackingNotificationAgent.Net;
using System.Net.Mail;

namespace LastDateOfAttendanceTrackingNotificationAgent
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var term = "";//DataAccess.GetCurrentTerm();
                term = "16/WI";
                //Skip it if there is no current term
                if (term == "") return;
                var usersToProcess = DataAccess.GetOutstandingLdoa(term);

                if (!usersToProcess.Any())
                {
                    var exchmail = new SmtpClient
                    {
                        Host = "exchmail.delta.edu",
                        Port = 25
                    };


                    var outGoing = new MailMessage {Sender = new MailAddress("error@delta.edu ")};

                    outGoing.To.Add(new MailAddress("webadmin@delta.edu"));
                    outGoing.Subject = "Last Date of Attendance Notification - zero outstanding";
                    outGoing.From = new MailAddress("error@delta.edu ");
                    outGoing.Body = "<p>There were zero outstanding notifications to process.</p>";
                    outGoing.IsBodyHtml = true;

                    exchmail.Send(outGoing);
                }

                foreach (var thisItem in usersToProcess)
                {
                    //Need to determine how long it has been since this has been run
                    if (thisItem.DateDropped == null) continue;
                    var daysSinceDrop = DateTime.Now.Subtract(thisItem.DateDropped.Value).Days;
                    var lastDate = thisItem.DateDropped.Value.AddDays(14);
                    //Remove this next line before production
                    //daysSinceDrop = 1;

                    SendEmail.NoticeType noticeToSend = SendSettings.GetNoticeType(daysSinceDrop);
                    var emailRecipients = new List<string> {"chadmichael@delta.edu", "michaelmccloskey@delta.edu"};

                    if (SendSettings.SendFacultyEmailOnDay(daysSinceDrop))
                    {
                        //First we need to get the instructors information
                        //emailRecipients.Add(thisItem.InstructorEmail);
                        //emailRecipients.Add("richardzeien@delta.edu");                        
                    }

                    //Next we need to get the division chairs information
                    var divisionChairEmail = "";

                    if (SendSettings.SendDivisionChairEmailOnday(daysSinceDrop))
                    {
                        //Next we need to get the division chairs information
                        divisionChairEmail = GetDivisionChairInfo.GetDivisionChairEmail(thisItem.CourseSection);

                        if (divisionChairEmail != "")
                        {
                            //emailRecipients.Add(divisionChairEmail);
                            //emailRecipients.Add("richardzeien@delta.edu");                        
                        }
                    }

                    if (emailRecipients.Any())
                    {
                        //SendEmail.SendNotice(noticeToSend, emailRecipients, thisItem.InstructorName, thisItem.StudentName, thisItem.StudentNumber.ToString(), thisItem.CourseSection, thisItem.DateDropped.Value.ToString("MM/dd/yyyy"), lastDate.ToString("MM/dd/yyyy"));
                        SendEmail.SendNotice(noticeToSend, emailRecipients, thisItem.InstructorName, thisItem.StudentName, thisItem.StudentNumber.ToString(), thisItem.CourseSection, thisItem.DateDropped.Value.ToString("MM/dd/yyyy"), lastDate.ToString("MM/dd/yyyy"), "DivChair: " + GetDivisionChairInfo.GetDivisionChairEmail(thisItem.CourseSection) + ", InsEmail: " + thisItem.InstructorEmail);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                //Send out an email.

                var exchmail = new SmtpClient
                {
                    Host = "exchmail.delta.edu",
                    Port = 25
                };


                var outGoing = new MailMessage {Sender = new MailAddress("error@delta.edu ")};

                outGoing.To.Add(new MailAddress("webadmin@delta.edu"));
                outGoing.Subject = "Error with Last Date of Attendance Notification";
                outGoing.From = new MailAddress("error@delta.edu ");
                outGoing.Body = "<p>" + ex.Message + "</p><p>" + ex.StackTrace + "</p>";
                outGoing.IsBodyHtml = true;                    
                                
                exchmail.Send(outGoing);
            }
        }
    }
}
