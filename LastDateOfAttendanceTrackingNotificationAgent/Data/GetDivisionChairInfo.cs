﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LastDateOfAttendanceTrackingNotificationAgent.Data
{
    public static class GetDivisionChairInfo
    {
        public static string GetDivisionChairEmail(string courseSection)
        {
            string email = "";
            using (ERPDataContext db = new ERPDataContext())
            {
                var result = db.GetDivisionChairEmail(courseSection.Split("-".ToCharArray())[0]).ToList();
                if (result.Count() > 0)
                {
                    email = result.First().DeltaEmail;
                    if (email == null)
                    {
                        email = "";
                    }
                }
            }
            
            return email;
        }
    }
}
