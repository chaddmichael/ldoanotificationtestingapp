﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LastDateOfAttendanceTrackingNotificationAgent.Data
{
    public static class DataAccess
    {
        public class UserInfo
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
        }

        public static List<GetUnfinishedLDOAResult> GetOutstandingLdoa(string term)
        {
            using (ERPDataContext db = new ERPDataContext())
            {
                var outstandingLODAs = db.GetUnfinishedLDOA(term);
                return outstandingLODAs.ToList();
            }
        }

        public static string GetCurrentTerm()
        {
            using(ODSDataContext db = new ODSDataContext())
            {
                var possibleTerms = db.View_TERMs.Where(X => X.TERMS_START_DATE < DateTime.Now && X.TERMS_END_DATE > DateTime.Now).OrderBy(X => X.TERMS_START_DATE);

                if (possibleTerms.Count() > 0)
                {
                    return possibleTerms.First().TERMS_ID;
                }
                else
                {
                    return "";
                }
            }
        }

    }
}
